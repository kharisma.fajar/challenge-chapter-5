// Create Variable
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const path = require('path');

//import module info (local module)
const time = require('./public/time.js');

// Create logging the request
app.use((req,res,next)=>{
    //show data from module info
    console.log(time()+' Logging the request');
    next();
});

//add Body-Parser and express.json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//For read file at folder 'public'
app.use(express.static(path.join(__dirname,'public')));

//For read extension type 'EJS'
app.set("view engine", "ejs");

//method get endpoint, Route to homepage
app.get('/',function(req,res){
    res.render('index');
});

//method get endpoint, Route to gamepage
app.get("/game",function(req,res){
    res.render('game');
});

//method get endpoint, Route to systemrequirements
app.get("/systemrequirements",function(req,res){
    res.render('systemrequirements');
});

//method get endpoint, Route to aboutUs
app.get("/gamefeature",function(req,res){
    res.render('gamefeature');
})

//method get endpoint, Route to login
app.get("/login",function(req,res){
    res.render('login');
});

//method get endpoint, Route to greet
app.get("/greet", (req, res) => {
    const name = req.query.name || "Kharis";
    res.render("greet", {
      name,
    });
  });

//Create server 8000
app.listen(8000,()=>{
    console.log('server started at port 8000');
});